
$(document).ready(function(){
  var left = true;
  console.log("ready");
    $("button#theme").click(function(){
      console.log("test");
      if (left) {
        document.body.style.backgroundImage = "url('https://i.ytimg.com/vi/O1ZIbCn8vT0/maxresdefault.jpg')";
        $(".bb").css({"background-color": "black", "box-shadow": "10px 10px 5px #39ff14", "color": "white"});
        left = false;
     }
     else {
        document.body.style.backgroundImage = "url('https://i.imgur.com/esYLx6b.jpg')";
         $(".bb").css({"background-color": "#999966", "box-shadow": "0px 0px 0px", "color": "black"});
         left = true;
     }
    });
    
    $.fn.center = function () {
      this.css("position","absolute");
      this.css("top", Math.max(0, (
        ($(window).height() - $(this).outerHeight()) / 2) +
         $(window).scrollTop()) + "px"
      );
      this.css("left", Math.max(0, (
        ($(window).width() - $(this).outerWidth()) / 2) +
         $(window).scrollLeft()) + "px"
      );
      return this;
    }

    $("#overlay").show();
    $("#overlay-content").show().center();

    setTimeout(function(){
      $("#overlay").fadeOut();
    }, 5000);

});
