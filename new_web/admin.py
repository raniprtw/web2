from django.contrib import admin
from .models import Activities, Subs

# Register your models here.
admin.site.register(Activities)
admin.site.register(Subs)
