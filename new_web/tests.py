from django.test import TestCase, Client
from django.urls import resolve
from .views import mssg
from .models import Activities
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.support.color import Color

# Create your tests here.


# class FunctionalTest(TestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(FunctionalTest, self).tearDown()

    # def test_input_todo(self):
    #     selenium = self.selenium
    #
    #     # link yang mau di test
    #     selenium.get('http://localhost:8000/')
    #     time.sleep(2)
    #
    #     # find the form element
    #     blank = selenium.find_element_by_name('messages') # belum tau mau isi apa
    #
    #     # tombol submit
    #     submit = selenium.find_element_by_id('submit')
    #
    #     # fill the form with data
    #     blank.send_keys('Coba Coba')
    #     time.sleep(2)
    #
    #     # submitting the form
    #     submit.send_keys(Keys.RETURN)
    #     time.sleep(2)

    # def test_layout(self):
    #     selenium = self.selenium
    #
    #     # link yang mau di test
    #     selenium.get('http://rani-and-friends.herokuapp.com/')
    #
    #     # nemuin tittle tulisan hello
    #     find_tag = selenium.find_element_by_tag_name('h2').text
    #     self.assertIn('ADD NEW MESSAGES', find_tag)
    #
    #     # ngecek tulisan di tombolnya
    #     find_button = selenium.find_element_by_id('submit').text
    #     self.assertIn('Submit', find_button)

    # def test_style(self):
    #     selenium = self.selenium
    #
    #     # ilnk yang mau di test
    #     selenium.get('http://rani-and-friends.herokuapp.com/')
    #
    #     # warna tombolnya
    #     button = selenium.find_element_by_id('submit').value_of_css_property('background-color')
    #     butt_color = Color.from_string(button).hex
    #     self.assertEqual('#007bff', butt_color)
    #
    #     # mengecek warna background
    #     background = selenium.find_element_by_id('testt').value_of_css_property('background-color')
    #     back_color = Color.from_string(background).hex
    #     self.assertEqual('#111111', back_color)

class Test(TestCase):
# Untuk ngecek dia ada atau ga si url nya
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)



     # Untuk ngecek apakah file html digunakan
    def test_template(self):
         response = Client().get('/')
         self.assertTemplateUsed(response, 'pesan.html')

    # Untuk ngecek function yang dipake ada atau engga
    def test_index_function(self):
         found = resolve('/')
         self.assertEqual(found.func, mssg) #index itu nama func


    # Dia ada tulisan "Hello, Apa kabar?"
    def test_greeting(self):
         new_response = self.client.get('/')
         html_response = new_response.content.decode('utf8')
         self.assertIn('Hello, Apa kabar?', html_response)

    def test_new_web_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('', {'messages': test})


        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_tempalte_profile(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile1.html')


    def test_url_books_is_exist(self):
        response = Client().get('/books')
        self.assertTemplateUsed(response, 'books.html')

    def test_find_function(self):
        response = Client().get('/api/books')
        self.assertEqual(response.status_code,200)

    def test_tempalte_subs(self):
        response = Client().get('/subs')
        self.assertTemplateUsed(response, 'story10.html')
