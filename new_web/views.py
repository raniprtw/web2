from django.shortcuts import render
from django.http import HttpResponse
from django.db import IntegrityError
from django.http import JsonResponse

from django.core.validators import validate_email
from .models import Activities, Subs
from .forms import Activities_Form, Subs_form
import urllib.request, json
import requests


# Fungsi buat nampilin PROFILEEEEs
def profile(request):
    return render(request, 'profile1.html')

# Buat masukin ke database
def mssg(request):
    #form = Activities_Form(request.POST or None)
    response = {}
    response['form'] = Activities_Form
    table = Activities.objects.all()
    response['table'] = table
    html = 'pesan.html'
    if(request.method == 'POST'):
        response['messages'] = request.POST['messages']
        ini_message = Activities(messages=response['messages'])
        ini_message.save()
        html = 'pesan.html'

    return render(request, html , response)

def books(request):
    return render(request,'books.html')

#buat return json dari hasil request ke api
def api(request):
    booklist = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    category = []
    for i in booklist['items']:
        data = {}
        data['poster'] = i['volumeInfo']['imageLinks']['smallThumbnail']
        data['title'] = i['volumeInfo']['title']
        data['author'] = ", ".join(i['volumeInfo']['authors'])
        data['publishDate'] = i['volumeInfo']['publishedDate']
        category.append(data)
    return JsonResponse({"data" : category} )

def subs(request):
    form = Subs_form(request.POST)
    return render(request,'story10.html',{'form': form})

def subscriber1(request):
    print("test subs baru")
    if request.method=='POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        try:
            p = Subs.objects.create(name=name, email=email, password=password)
        except IntegrityError as e:
            return JsonResponse({'status': False})

    return JsonResponse({'name': name, 'email': email, 'status': True})

def validation(request):
    if request.method=='POST':
        email = request.POST['email']
        a = Subs.objects.filter(email=email)
        if a.exists():
            return JsonResponse({'isExist': True})
        else:
            return JsonResponse({'isExist': False})
