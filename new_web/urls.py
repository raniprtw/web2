from django.conf.urls import url
from .views import *


urlpatterns = [
    url('profile', profile, name='profile'),
    url('subs1', subscriber1, name="subscriber1"),
    url('validation', validation, name='validation'),
    url('subs', subs, name='subs'),
    url('api/', api, name='api'),
    url('books', books, name='books'),
    url('', mssg),
    url('mssg', mssg, name='mssg'),
]
