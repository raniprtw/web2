from django import forms
from .models import Activities

class Activities_Form(forms.Form):

    error_messages = {
        'required': 'Please type',
    }

    mssg_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Leave a message!',
    }

    messages = forms.CharField(label='',  required=True,
        max_length=300, widget=forms.TextInput(attrs=mssg_attrs))

class Subs_form(forms.Form):
    names = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'required',
        'id': 'name',
    }

    emails = {
        'type': 'email',
        'class': 'form-control',
        'placeholder': 'required',
        'id':'email',
    }

    passwords = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'required',
        'id': 'password',
    }

    name = forms.CharField(max_length = 50, error_messages={"required": "Please enter your name"}, widget = forms.TextInput(attrs=names))
    password = forms.CharField(max_length = 15, min_length=8, error_messages={"min_length": "password must be longer than 5 characters"}, widget = forms.PasswordInput(attrs=passwords))
    email= forms.EmailField(max_length=50, error_messages={"required": "Please enter a valid email"}, widget=forms.TextInput(attrs=emails))
