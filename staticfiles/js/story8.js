$(document).ready(function(){
    var acc = $('.accordion');
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
});

$(document).ready(function(){
  var left = true;
    $("#theme").click(function(){
      if (left) {
        document.body.style.backgroundImage = "url('https://i.ytimg.com/vi/O1ZIbCn8vT0/maxresdefault.jpg')";
        $(".lesgo").css({"background-color": "black", "box-shadow": "10px 10px 5px #39ff14", "color": "white"});
        left = false;
     }
     else {
        document.body.style.backgroundImage = "url('https://i.imgur.com/esYLx6b.jpg')";
         $(".lesgo").css({"background-color": "#999966", "box-shadow": "0px 0px 0px", "color": "black"});
         left = true;
     }
    });
});
